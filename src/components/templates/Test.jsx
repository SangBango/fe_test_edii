import React, { Component } from 'react'
import Navbar from '../templates/Navbar'
import Sidebar from '../templates/Sidebar'
import Footer from '../templates/Footer'
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

class Test extends Component {
    constructor() {
        super();

        this.state = {
            id_platform: "",
            nama_platform: "",
            doc_type: "",
            term_of_payment: ""
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();

        const addBooking = "http://localhost:8085/add_booking_request"

        const booking = {
            id_platform: this.state.id_platform,
            nama_platform: this.state.nama_platform,
            doc_type: this.state.doc_type,
            term_of_payment: this.state.term_of_payment
        }

        axios.post(addBooking, booking, {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
            }
        })
        .then((response) => {
            let res = response.data;
            
            toast.info('success', 
            {
              position: toast.POSITION.TOP_CENTER,
              hideProgressBar: true,
              className: "custom-toast",
              autoClose: 2000,
            })
        })
        .catch((error) => {
            console.log(error)
        });
    }

    componentDidMount() {

        const urlGetBooking = "http://localhost:8085/all_booking_request"

        axios.get(urlGetBooking, {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
            }
        })
        .then((response) => {
            let res = response.data;
            this.setState ({
                bookings: res
            })

            const script = document.createElement("script");

            script.src = "js/main.js";
            script.async = true;
      
            document.body.appendChild(script);            
        })
        .catch((error) => {
            console.log(error)
        });
    }

    render() {
        return (
            <div>
                <Navbar></Navbar>
                <Sidebar></Sidebar>
                <div className="content-wrapper">
                    {/* Content Header (Page header) */}
                    <section className="content-header">
                    <div className="container-fluid">
                        <div className="row">
                        <div className="col-md-5">
                            {/* general form elements disabled */}
                            <div className="card card-dark">
                            <div className="card-header">
                                <h3 className="card-title" style={{color: "#fff"}}>General Elements</h3>
                            </div>
                            {/* /.card-header */}
                            <div className="card-body">
                                <form onSubmit={this.onSubmit}>
                                <div className="row">
                                    <div className="col-sm-8">
                                    {/* text input */}
                                    <div className="form-group">
                                        <label>id_platform</label>
                                        <input
                                        type="text"
                                        className="form-control"
                                        name="id_platform"
                                        id="id_platform"
                                        onChange={this.onChange}
                                        value={this.state.id_platform}
                                        required
                                        placeholder="Enter ..."
                                        />
                                    </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-12">
                                    {/* text input */}
                                    <div className="form-group">
                                        <label>nama_platform</label>
                                        <input
                                        type="text"
                                        className="form-control"
                                        name="nama_platform"
                                        id="nama_platform"
                                        onChange={this.onChange}
                                        value={this.state.nama_platform}
                                        required
                                        placeholder="Enter ..."
                                        />
                                    </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6">
                                    <div className="form-group">
                                        <label>doc_type</label>
                                        <input
                                        type="text"
                                        className="form-control"
                                        name="doc_type"
                                        id="doc_type"
                                        onChange={this.onChange}
                                        value={this.state.doc_type}
                                        required
                                        placeholder="Enter ..."
                                        />
                                    </div>
                                    </div>
                                    <div className="col-sm-6">
                                    <div className="form-group">
                                        <label>term_of_payment</label>
                                        <input
                                        type="text"
                                        className="form-control"
                                        name="term_of_payment"
                                        id="term_of_payment"
                                        onChange={this.onChange}
                                        value={this.state.term_of_payment}
                                        required
                                        placeholder="Enter ..."
                                        />
                                    </div>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-primary">
                                    SUBMIT
                                </button>
                                </form>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </section>

                    <table
                        style={{ fontSize: "12px", width: "100%" }}
                        id="products"
                        className="table table-bordered table-hover"
                    >
                        <thead>
                        <tr>
                            <th>id_platform</th>
                            <th>nama_platform</th>
                            <th>doc_type</th>
                            <th>term_of_payment</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.bookings.map((booking, i) => (
                            <tr key={i}>
                            <td>{booking.id_platform}</td>
                            <td>{booking.nama_platform}</td>
                            <td>{booking.doc_type}</td>
                            <td>Rp{booking.term_of_payment}</td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
                <Footer></Footer>
            </div>
        )
    }
}
export default Test;